import sys
import numpy as np
from scipy import interpolate
from scipy import special as sp
import time
import scipy.fftpack as fft
import scipy.constants as co
import petsc4py
petsc4py.init(sys.argv)

from petsc4py import PETSc
from mpi4py import MPI


BC_COEFF = {'neu': 1, 'dir': -1}
DEFAULT_BC = ['dir', 'dir', 'neu', 'dir']
bc = DEFAULT_BC
nx = 10000
ny = 1200
lx = 25.e-2
ly = 3.e-2
dz = lx/nx
dr = ly/ny


da = PETSc.DMDA().create(dim=2, dof=1, sizes=(nx, ny),
                          boundary_type=(1,1),
                          stencil_type=PETSc.DMDA.StencilType.STAR,
                          stencil_width=1)
da.setUniformCoordinates()


# Some global values for Hankel Transform
Rc = np.arange(0. + dr/2.,ly,dr)
Ntotal = 500
zeros = sp.jn_zeros(0,Ntotal)
R = lx + 5.e-2
Omega = zeros[-1]/R
omega_m = zeros/R
k = omega_m
r_k = zeros/Omega


#####################Electric potential context######################
ksp = PETSc.KSP().create()

def ComputeMatrix(ksp, J, jac):

    dm = ksp.getDM()
    mx, my = dm.getSizes()

    Hx        = lx/ mx
    Hy        = ly / my
    HxdHy     = Hx / Hy
    HydHx     = Hy / Hx

    ((xs, ys), (xm, ym)) = dm.getCorners()


    for j in range(ys, ys + ym):
        for i in range(xs, xs + xm):
            # We assume that y is the radial coord.
            r = (j + 0.5) * Hy

            stencil =  ((-HxdHy,               0,  -1),
                        (-HydHx,              -1,   0),
                        (2.0*(HxdHy + HydHx),  0,   0),
                        (-HydHx,              +1,   0),
                        (-HxdHy,               0,  +1),
                        (-0.5*Hx/r,            0,  +1),
                        (0.5*Hx/r,             0,  -1))

            row, col = PETSc.Mat.Stencil(), PETSc.Mat.Stencil()
            row.i, row.j = i, j

            for (v, di, dj) in stencil:
                col.i, col.j = i + di, j + dj

                # Apply boundary conditions
                if i == 0 and di == -1:
                    col.i = i
                    v = BC_COEFF[bc[0]] * v

                if i == mx - 1 and di == 1:
                    col.i = i
                    v = BC_COEFF[bc[1]] * v

                if j == 0 and dj == -1:
                    col.j = j
                    v = BC_COEFF[bc[2]] * v

                if j == my - 1 and dj == 1:
                    col.j = j
                    v = BC_COEFF[bc[3]] * v

                jac.setValueStencil(row, col, v, PETSc.InsertMode.ADD_VALUES)


    jac.assemblyBegin(PETSc.Mat.AssemblyType.FINAL_ASSEMBLY)
    jac.assemblyEnd(PETSc.Mat.AssemblyType.FINAL_ASSEMBLY)


ksp.setComputeOperators(ComputeMatrix)
ksp.setDM(da)
ksp.setFromOptions()
ksp.setUp()

# Setting solver and preconditioner
ksp.setType('ibcgs')
pc = ksp.getPC()
pc.setType('bjacobi')

localx = da.createLocalVector()
m, p = ksp.getOperators()



def DDM(ksp, m, rhs, state, label, mu=None):

    zc = state.grid.x.centers
    rc = state.grid.y.centers
    zf = state.grid.x.nodes
    rf = state.grid.y.nodes

    ############################################
    # Setting source processes for broadcasting#
    ############################################
    source0, sourceRmaxL = source(zf,rf)

    b = da.createGlobalVector()

    computeRHS(b, rhs)

    ksp.setInitialGuessNonzero('True')
    IG = state.problem_data[label[0]]
    x = solve(ksp, m, b, IG)
    state.problem_data[label[0]] = da.getVecArray(x)[:,:]

    # This is for the processes where these vectors are not going to be computed
    phi_gamma = None
    I_L = None
    am = None
    phi_zL = None
    F = None

    # Building a communicator for processes owning the rmax boundary
    ############################################################################
    ############################################################################
    world_rank = MPI.COMM_WORLD.Get_rank()

    if rf[-1] == ly:
        color = 55
        #key = -world_rank
        key = 0
    else:
        color = 77
        #key = +world_rank
        key = 0

    comm_bound = MPI.COMM_WORLD.Split(color,key)


    ############################################################################
    # Free B.C. in r=ly
    ############################################################################
    if rf[-1] == ly:
        phi = da.getVecArray(x)[:, :]
        dphi_drho = np.gradient(phi, dr, axis=1)

        # We set a b.c. in rmax that is compatible with the b.c. in zmin and zmax.
        ############################################################################
        ############################################################################

        dphi_interp = interpolate.RectBivariateSpline(zc,rc,dphi_drho)

        # "Flux equation"
        N = int(nx) # Number of points in the k-space
        km =  np.pi * np.arange( 1, N + 1 ) / lx

        arg = km * ly

        factor = sp.i1e( arg ) / sp.i0e( arg ) + sp.k1e( arg )/sp.k0e( arg )
        factor *= arg * lx / (2.*ly)

        dphi_drho =  dphi_interp.ev( zc, ly )

        dphi_drho = scattertoall(dphi_drho,comm_bound)
        bm = - 0.5 * fft.dst(dphi_drho,type=2) * dz

        am = bm / factor

        am_aux = np.zeros(am.shape)
        am_aux[-1] = am[-1]

        phi_gamma =  0.5 * fft.dst(am,type=3) - fft.dst(am_aux,type=3)

        ((xm,xe),(ym,ye)) = da.getRanges()

        phi_gamma = phi_gamma[xm:xe]

        ############################################################################
        ############################################################################
        # Modification of the r.h.s in order to include the BC at rmax
        apply_inhom_bc( b, phi_gamma)

    comm_bound.Free()

    ksp.setInitialGuessNonzero('True')
    IGDM = state.problem_data[label[1]]
    x = solve(ksp, m, b, IGDM)

    state.problem_data[label[1]]= da.getVecArray(x)[:,:]


    ############################################################################
    # Free B.C. in z=L
    ############################################################################
    ############################################################################
    # Communicator for z=L
    ############################################################################
    if zf[-1] == lx:
        color = 66
        #key = -world_rank
        key = 1
    else:
        color = 88
        #key = +world_rank
        key = 1
    commL = MPI.COMM_WORLD.Split(color,key)
    ############################################################################
    ############################################################################

    # Broadcast am that will be needed afterwards
    am = MPI.COMM_WORLD.bcast(am,sourceRmaxL)

    phi1 = da.getVecArray(x)[:,:]

    if zf[-1]==lx:
        dphi1_L = -2 * phi1[-1, :] / dz
        dphi1_L = scattertoall(dphi1_L,commL)
        I_L = Integral(dphi1_L,am)

    commL.Free()

    I_L = MPI.COMM_WORLD.bcast(I_L,sourceRmaxL)

    computeRHS(b, rhs)

    if zf[-1]==lx:
        F = 0.5 * (1 - np.exp(-2*k*lx))*I_L/k
        phi_zL = Hankel(F,rc,R)
        apply_inhom_bcz(b,phi_zL,zf[-1])


    F = MPI.COMM_WORLD.bcast(F,sourceRmaxL)


    if rf[-1]==ly:
        f_0 = F[...,None] /(1 - np.exp(-2*k[...,None]*lx))
        f_0 = f_0*(np.exp(k[...,None]*(zc[None,...]-lx))-np.exp(-k[...,None]*(zc[None,...]+lx)))
        phibar_0_gamma = Hankel_rev(f_0,rf[-1],R).reshape(f_0.shape[1])

        phi_gamma += phibar_0_gamma
        apply_inhom_bc(b,phi_gamma)


    IGDM1 = state.problem_data['IGDM1']
    ksp.setInitialGuessNonzero('True')
    x = solve(ksp, m, b, IGDM1)
    state.problem_data['IGDM1'] = da.getVecArray(x)[:,:]


    phib = np.array((phi_zL,phi_gamma))

    return x, phib



def source(zf,rf):
    if (zf[0]==0. and rf[-1]==ly):
        source0 = PETSc.COMM_WORLD.getRank()
    else:
        source0 = None

    source0 = MPI.COMM_WORLD.Get_size()*[source0]
    source0 = MPI.COMM_WORLD.alltoall(source0)

    for root in source0:
        if root is not None:
            source0 = root
            break

    if (zf[-1]==lx and rf[-1]==ly):
        sourceRmaxL = PETSc.COMM_WORLD.getRank()
    else:
        sourceRmaxL = None

    sourceRmaxL = MPI.COMM_WORLD.Get_size()*[sourceRmaxL]
    sourceRmaxL = MPI.COMM_WORLD.alltoall(sourceRmaxL)

    for root in sourceRmaxL:
        if root is not None:
            sourceRmaxL = root
            break

    return np.array((source0,sourceRmaxL))



def Hankel(f,x,R):

    Y = 2 * sp.j0(x[...,None]*zeros[None,...]/R)/(R**2 * sp.j1(zeros[None,...])**2)
    return np.sum(Y*f[None,...],axis=1)



def Hankel_rev(f,x,R):

    Y = 2 * sp.j0(x[...,None]*zeros[None,...]/R)/(R**2 * sp.j1(zeros[None,...])**2)
    return np.sum(Y[...,None]*f[None,...],axis=1)



def Integral(dphi1_L,am):

    N = int(nx) # Number of points in the k-space
    km =  np.pi * np.arange( 1, N + 1 ) / lx
    s = (-1)**np.arange( 1, N + 1 )
    f_dphi1_L = interpolate.interp1d(np.r_[0, Rc], np.r_[dphi1_L[0], dphi1_L],
                                     bounds_error=False, fill_value=np.nan)

    def f_dphi2_L(r):
        factor = km * (ly - r[..., None])
        factor = np.exp(factor)
        return np.sum(s * am * km *
                      sp.k0e(km * r[..., None]) / sp.k0e(km * ly) *
                      factor,
                      axis=-1)

    def f_dphi_L(r):
        return np.where(r <= Rc[-1], f_dphi1_L(r), f_dphi2_L(r))



    return - Hankel(f_dphi_L(r_k),omega_m,Omega)


def computeRHS(b, rhs):

    Hx, Hy    = lx/nx, ly/ny

    array = da.getVecArray(b)
    array[:, :] = rhs * Hx * Hy



def apply_inhom_bc(b, phi_gamma):
    """ Modifies q to apply an inhomogeneous b.c. at the external side of the
    cylinder.  """
    #
    # We have to use that when j + 1 falls outside the domain
    # phi[i, j + 1] = 2 b[i] - phi[i, j]  (Dirichlet b.c)
    # This adds a contribution to q[i, j] equal to 2 b[i] times the
    # coefficient that would multiply phi[i, j + 1] (i.e. di=0, dj=1) in the
    # stencil defined in compute_matrix.  This is -HydHx - 0.5 * Hx / r.
    Hx, Hy    = lx/nx, ly/ny
    HxdHy     = Hx / Hy
    HydHx     = Hy / Hx
    r = (ny - 0.5) * Hy

    rhs = da.getVecArray(b)
    rhs[:,ny-1] -= 2 * (-HxdHy - 0.5 * Hx / r) * phi_gamma



def apply_inhom_bcz( b, bz, zf):
    """ Modifies rhs to apply an inhomogeneous b.c. at the external side of the
    cylinder.  """
    #
    # We have to use that when j + 1 falls outside the domain
    # phi[i, j + 1] = 2 b[i] - phi[i, j]  (Dirichlet b.c)
    # This adds a contribution to q[i, j] equal to 2 b[i] times the
    # coefficient that would multiply phi[i, j + 1] (i.e. di=0, dj=1) in the
    # stencil defined in compute_matrix.  This is -HydHx - 0.5 * Hx / r.
    Hx, Hy    = lx / nx, ly / ny
    HxdHy     = Hx / Hy
    HydHx     = Hy / Hx

    rhs = da.getVecArray(b)

    rhs[nx-1] -= 2. * (-HydHx) * bz



def efield(rhs, state, edges=None):

    label = ('IG', 'IGDM')
    x, phib =  DDM(ksp, m,  rhs, state, label)


    da.globalToLocal(x, localx)
    phi = da.getVecArray(localx)
    mx, my = nx, ny
    hx, hy = lx/nx, ly/ny
    (xs, xe), (ys, ye) = da.getRanges()

    # Boundary conditions
    if xs==0:
        v =  BC_COEFF[bc[0]]
        phi[-1,ys:ye] =  v * phi[0,ys:ye]

    if ys==0:
        v =  BC_COEFF[bc[2]]
        phi[xs:xe,-1] = v * phi[xs:xe,0]
    if xe==mx:
        v = BC_COEFF[bc[1]]
        phi[xe,ys:ye] = 2.*phib[0] + v * phi[xe-1,ys:ye]

    if ye==my:
        v = BC_COEFF[bc[3]]
        phi[xs:xe,ye] = 2.*phib[1] + v * phi[xs:xe,ye-1]

    if edges is not None:
        Ezemin = - (phi[xs:xe+1,ys:ye] - phi[xs-1:xe,ys:ye])/hx
        Eremin = - ( phi[xs:xe,ys:ye+1] - phi[xs:xe,ys-1:ye])/hy

    Ez = - (phi[xs+1:xe+1,ys:ye] - phi[xs-1:xe-1,ys:ye])/(2.*hx)
    Er = - (phi[xs:xe,ys+1:ye+1] - phi[xs:xe,ys-1:ye-1])/(2.*hy)

    if edges is not None:
        return Ez, Er, Ezemin, Eremin
    else:
        return Ez, Er



def scattertoall(a,comm_bound):

    c = PETSc.Vec().createWithArray(a,comm=comm_bound)
    scatter, Vecscatter = PETSc.Scatter.toAll(c)
    scatter.scatter(c, Vecscatter, False, PETSc.Scatter.Mode.FORWARD)
    safeVecscatter = Vecscatter.getArray().copy()

    # Not sure if they must be destroyed
    scatter.destroy()
    Vecscatter.destroy()


    return safeVecscatter



def solve(ksp, m, b, IG=None):

    x = m.createVecLeft()
    if IG is not None:
        x.setArray(IG)

    ksp.solve(b, x)

    return x


if __name__ == '__main__':
    DDM()
