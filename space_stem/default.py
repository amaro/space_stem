from numpy import *

import numpy as np
import scipy.constants as co

import chemise as ch

MUN_E = 1e24

NAIR = co.value('Loschmidt constant (273.15 K, 101.325 kPa)')


class HumidAir(ch.ReactionSet):



    DEFAULT_REDUCED_MOBILITIES = {

        'e'   : MUN_E}



    def __init__(self, pw=0.015, extend=False):

        super(HumidAir, self).__init__()


        self.pw = pw



        # These are ignored products

        self.fix({

            'OH' : 0.0,

            'H': 0.0,

            'N2O': 0.0,

            'O3': 0.0,

            'O(3P)': 0.0,

            'O(1S)': 0.0,

            'N2(A)': 0.0,

            'N2(ap)': 0.0,

            'M': NAIR,

            'O2': 0.21*NAIR,

            'N2': 0.79*NAIR,

            'H2O': pw*NAIR,

            'O': 0.0})



        self.add("e + N2 -> 2 * e + N2+",

                 ch.LogLogInterpolate0("chemise/swarm/k025.dat", extend=extend))



        self.add("e + O2 -> 2 * e + O2+",

                 ch.LogLogInterpolate0("chemise/swarm/k042.dat", extend=extend))



        self.add("e + O2 + O2 -> O2- + O2",

                 att(1.4e-29*co.centi**6),ref='Liu2017')



        self.add("e + O2 -> O + O-",

                 ch.LogLogInterpolate0("chemise/swarm/k027.dat"), extend=extend)



        self.add("M + O2- -> e + O2 + M",

                 PancheshnyiFitEN(1.24e-11 * co.centi**3, 179, 8.8),

                 ref="Pancheshnyi2013/JPhD")



        self.add("O2 + O- -> O2- + O",

                 PancheshnyiFitEN(6.96e-11 * co.centi**3, 198, 5.6),

                 ref="Pancheshnyi2013/JPhD")



        self.add("N2 + O- -> e + N2O",

                 PancheshnyiFitEN(1.16e-12 * co.centi**3, 48.9, 11),

                 ref="Pancheshnyi2013/JPhD")



        self.add("O2 + O- + M -> O3- + M",

                 PancheshnyiFitEN2(1.1e-30 * co.centi**6, 65),

                 ref="Pancheshnyi2013/JPhD")



        # This is the g in Gallimberti 1979 fitted to experimental data

        # and transformed into Td / K.  See scratch/gallimberti/gallimberti.py

        g = 0.18



        self.add2("O2- + H2O + M -> O2^-.(H2O) + M",

                  ch.Constant(2.2e-28 * co.centi**6),

                  Gallimberti(2.2e-28 * co.centi**6, NAIR,

                              -18.44 * co.kilo * co.calorie / co.N_A, g),

                  #ch.Constant(2e-22 * co.centi**3),

                  ref="Gallimberti1979/JPhys")



        self.add2("O2^-.(H2O) + H2O + M -> O2^-.(H2O)2 + M",

                  ch.Constant(5e-28 * co.centi**6),

                  Gallimberti(5e-28 * co.centi**6, NAIR,

                              -8.35 * co.kilo * co.calorie / co.N_A, g),

                  #ch.Constant(1.1e-14 * co.centi**3),

                  ref="Gallimberti1979/JPhys")



        self.add2("O2^-.(H2O)2 + H2O + M -> O2^-.(H2O)3 + M",

                  ch.Constant(5e-29 * co.centi**6),

                  Gallimberti(5e-29 * co.centi**6, NAIR,

                              -6.46 * co.kilo * co.calorie / co.N_A, g),

                  #ch.Constant(9e-15 * co.centi**3),

                  ref="Gallimberti1979/JPhys")



        self.add("N2+ + N2 + M -> N4+ + M",

                 TemperaturePower(5e-29 * co.centi**6, 3),

                 ref="Aleksandrov1999/PSST")



        self.add("N4+ + O2 -> 2 * N2 + O2+",

                 TemperaturePower(2.5e-10 * co.centi**3, 3),

                 ref="Aleksandrov1999/PSST")



        self.add("O2+ + O2 + M -> O4+ + M",

                 TemperaturePower(2.4e-30 * co.centi**6, 3),

                 ref="Aleksandrov1999/PSST")



        self.add("O2+ + H2O + M -> O2+.(H2O) + M",

                 ch.Constant(2.6e-28 * co.centi**6),

                 ref="Aleksandrov1999/PSST")



        self.add("O2+.(H2O) + H2O -> H3O+ + OH + O2",

                 ch.Constant(3e-10 * co.centi**3),

                 ref="Aleksandrov1999/PSST")



        for n in range(3):

            self.add("%s + H2O + M -> %s + M" % (cluster(n), cluster(n + 1)),

                     ch.Constant(3e-27 * co.centi**6),

                     ref="Aleksandrov1999/PSST")



        self.add("e + H3O+.(H2O)3 -> H + 4 * H2O",

                 ETemperaturePower(6.5e-6 * co.centi**3, 0.5,

                                   "chemise/swarm/meanenergy.dat", extend=extend),

                                   ref="Aleksandrov1999/PSST")



        self.add("e + O4+ -> ",

                 ch.Interpolate0("chemise/swarm/rec_electron.dat", zero_value=0.0,

                                 extend=extend))


        ########################################################################
        # Excitation of electronic states
        ########################################################################

        self.add("e + N2 -> e + N2(B)",

                 ch.LogLogInterpolate0("chemise/swarm/k018.dat", extend=extend))

        self.add("e + N2 -> e + N2(C)",

                 ch.LogLogInterpolate0("chemise/swarm/k010.dat", extend=extend))

        # Radiative decay - Quenching factor 0.038
        self.add("N2(C) -> g1 + N2(B)",

                 ch.Constant(2.47e7),ref="Capitelli/Book") # 2PN2

        self.add("N2(B) -> g2 + N2(A)",

                 ch.Constant(1.34e5),ref='Capitelli/Book') # 1PN2

        ########################################################################
        ########################################################################

        # Quenching
        self.add("N2(B) + N2 -> 2*N2",

                 ch.Constant(2.e-12*co.centi**3),ref="Capitelli")

        self.add("N2(B) + O2 -> N2 + 2*O",

                 ch.Constant(3.e-10*co.centi**3),ref="Capitelli/Book")

        self.add("N2(C) + N2 -> N2(ap) + N2",

                 ch.Constant(1.e-11*co.centi**3),ref="Capitelli/Book")

        self.add("N2(C) + O2 -> N2 + O(3P) + O(1S)",

                 ch.Constant(3.e-10*co.centi**3),ref="Capitelli/Book")

        ########################################################################
        ########################################################################


        # Add bulk recombination reactions

        pos = [s for s in self.species if '+' in s]

        neg = [s for s in self.species if '-' in s]

        self.add_pattern("{pos} + {neg} -> ",

                         {'pos': pos, 'neg': neg},

                         ch.Constant(1e-7 * co.centi**3),

                         generic="A+ + B- -> ",

                         ref="Kossyi1992/PSST")



        self.initialize()  # Adding reactions


def cluster(n):

    if n == 0:

        return "H3O+"

    if n == 1:

        return "H3O+.(H2O)"

    else:

        return "H3O+.(H2O)%d" % n


class TemperaturePower(ch.Rate):

    def __init__(self, k0, power, T0=300):

        self.k0 = k0

        self.power = power

        self.T0 = T0



    def __call__(self, EN, T):

        return full_like(EN, self.k0 * (self.T0 / T)** self.power)



    def latex(self):

        return (r"$\num{%g} \times (\num{%g} / T)^{\num{%g}}$"

                % (self.k0, self.T0, self.power))


class ETemperaturePower(ch.LogLogInterpolate0):

    def __init__(self, k0, power, *args, **kwargs):

        self.k0 = k0

        self.power = power

        self.T0 = kwargs.get('T0', 300)



        super(ETemperaturePower, self).__init__(*args, **kwargs)





    def __call__(self, EN, T):

        Te = T + 3648.6 * (EN)**0.46

        return full_like(EN, self.k0 * (self.T0 / Te)** self.power)



    def latex(self):

        return (r"$\num{%g} \times (\num{%g} / T_e)^{\num{%g}}$"

                % (self.k0, self.T0, self.power))


class PancheshnyiFitEN(ch.Rate):

    def __init__(self, k0, a, b):

        self.k0 = k0

        self.a = a

        self.b = b



    def __call__(self, EN, T):

        return self.k0 * exp(-(self.a / (self.b + EN))**2)



    def latex(self):

        self.k_0 = self.k0

        params = ["$%s = \\num{%g}$" % (s, getattr(self, s))

                  for s in ('k_0', 'a', 'b')]



        return ("$k_0e^{-\\left(\\frac{a}{b + E/n}\\right)^2}$ [%s]"

                % ", ".join(params))


class PancheshnyiFitEN2(ch.Rate):

    def __init__(self, k0, a):

        self.k0 = k0

        self.a = a



    def latex(self):

        self.k_0 = self.k0

        params = ["$%s = \\num{%g}$" % (s, getattr(self, s))

                  for s in ('k_0', 'a')]



        return ("$k_0e^{-\\left(\\frac{E/n}{a}\\right)^2}$ [%s]"

                % ", ".join(params))



    def __call__(self, EN, T):

        return self.k0 * exp(-(EN / self.a)**2)


class Gallimberti(ch.Rate):

    def __init__(self, kforward, n0, deltag, g):

        self.k0 = kforward * n0

        self.deltag = deltag

        self.g = g

    def latex(self):

        params = ["$%s = \\num{%g}$" % (s, v)

                  for s, v in (('k_0', self.k0),

                               ('\Delta G', self.deltag),

                               ('g', self.g))]



        return ("$k_0 e^{\\left(\\frac{\Delta G}{kT_i}\\right)}; T_i=T + \\frac{1}{g}\\frac{E}{n}$ [%s]"

                % ", ".join(params))


    def __call__(self, EN, T):

        Ti = T + EN / self.g

        return self.k0 * exp(self.deltag / (co.k * Ti))


class att(ch.Rate):
    def __init__(self, a):

        self.a = a

    def __call__(self, EN, T):

        Te = T + 3648.6 * (EN)**0.46

        return self.a*np.exp(700.*(1/T - 1/Te))/((Te/300.)*np.exp(600./T))


#-------------------------------------------------------------------------------

rs = HumidAir()

def chemical(state,en,tgas):

    dn_dt = rs.fderivs(state.q[4:], en,tgas)

    return dn_dt


class qtp_args(object):

    def __init__(self):
        super(qtp_args,self).__init__()

        self.qtp_path = qtp_path
        self.initialize()



if __name__ == '__main__':

    main()
