import yt
from clawpack.pyclaw import Solution
import yt
from clawpack.pyclaw import Solution
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import numpy as np
import matplotlib.pyplot as plt

frame = 4 # Frame to plot (0-10)
plot_all_quadrants = False  # If True, reflect data to plot all four y-z plane quadrants

# Load solution
sol = Solution(frame,path='./_output/',file_format='hdf');

x,y,z = sol.grid.dimensions

if plot_all_quadrants:
    # Reflect solution in y and z
    y_reflection = sol.q[:,:,::-1,:]
    double = np.concatenate((y_reflection,sol.q),2)
    z_reflection = double[:,:,:,::-1]
    full = np.concatenate((z_reflection,double),3)
    bbox = np.array([[x.lower,x.upper],[-y.upper,y.upper],[-z.upper,z.upper]])
else:
    full = sol.q
    bbox = np.array([[x.lower,x.upper],[y.lower,y.upper],[z.lower,z.upper]])

# Set up field dictionary that yt expects
data = {'Density'    : full[0,:,:,:],
        'x-momentum' : full[1,:,:,:],
        'y-momentum' : full[2,:,:,:],
        'z-momentum' : full[3,:,:,:],
        'Energy'     : full[4,:,:,:]}

pf = yt.load_uniform_grid(data,full[0,:,:,:].shape, 1, bbox=bbox)

# Plot a cut plane: specify normal vector and center, in that order
cp = pf.h.cutting([0.0, 1.0, 0.0], [0.75,0.,0.25])
pw = cp.to_pw(fields = ["Density"])
pw.show()
