#!/usr/bin/env python
# encoding: utf-8
r"""
Compressible Euler flow in cylindrical symmetry
===============================================

Solve the Euler equations of compressible fluid dynamics in 2D r-z coordinates:

.. math::
    \rho_t + (\rho u)_x + (\rho v)_y & = - \rho v / r \\
    (\rho u)_t + (\rho u^2 + p)_x + (\rho uv)_y & = -\rho u v / r \\
    (\rho v)_t + (\rho uv)_x + (\rho v^2 + p)_y & = - \rho v^2 / r \\
    E_t + (u (E + p) )_x + (v (E + p))_y & = - (E + p) v / r.

Here :math:`\rho` is the density, (u,v) is the velocity, and E is the total energy.
The radial coordinate is denoted by r.
"""
import numpy as np
import scipy.constants as co
from scipy import interpolate

import euler_drift # Riemann solver
import poisson_sparse as poisson # Electromagnetic module
import default as chemistry # Chemistry module
from clawpack import riemann

from mpi4py import MPI

# Labels for fluid equations
density = 0
x_momentum = 1
y_momentum = 2
energy = 3

euler_num_eqn = 4
nspecies = chemistry.rs.nspecies
num_eqn = euler_num_eqn + nspecies

# Space parameters
dimx = 10000
dimy = 1200
az = 0.
ar = 0.
bz = 25.e-2
br = 3.e-2
dz = (bz - az)/dimx
dr = (br - ar)/dimy

z0 = 5.e-2

sigma = 0.3e-2

# Average molecular weight of air
mair = 28.97 * co.gram / co.N_A

# Number density of air
nair = co.value('Loschmidt constant (273.15 K, 101.325 kPa)')

# Mass density of air
rhoair = mair * nair

# Mobility of species
dspecies = chemistry.rs.dspecies
mu = np.empty(euler_num_eqn + 1)
muspecies = chemistry.rs.species_vector(chemistry.rs.DEFAULT_REDUCED_MOBILITIES)
mu[4] = muspecies[dspecies['e']]/nair

# Useful labels
e = dspecies['e'] + 4
O2p = dspecies['O2+'] + 4
N2p = dspecies['N2+'] + 4

# Sign of the electric charge
charge_sign = chemistry.rs.species_vector( {

    'e'   : -1,

    'O-'  : -1,

    'O2-' : -1,

    'O3-' : -1,

    'O4+' : 1,

    'N4+' : 1,

    'N2+' : 1,

    'O2+' : 1,

    'O2^-.(H2O)': -1,

    'O2^-.(H2O)2': -1,

    'O2^-.(H2O)3': -1,

    'O2+.(H2O)': 1,

    'H3O+': 1,

    'H3O+.(H2O)': 1,

    'H3O+.(H2O)2': 1,

    'H3O+.(H2O)3': 1,

})

# Useful constants
gamma = 1.4 # Ratio of specific heats
eta = 0.5 # Fraction of the dissipated energy that goes into gas heating
epsilon_0 = co.epsilon_0
charge = co.e


def qinit(state):


    grid = state.grid
    z, r = grid.p_centers

    temperature = 300.
    temperature += 2700. * np.exp( -np.maximum(z-z0,0)**2/( 2.0 * sigma**2 )
                                - r**2 / (2.0 * sigma**2))

    pressure = co.k * nair * temperature

    state.q[density] = mair * nair
    state.q[x_momentum] = 0.0
    state.q[y_momentum] = 0.0

    # For nonzero velocity we need also the mechanical energy term.
    state.q[energy] = (pressure / (gamma - 1.))

    # Initialize number density of charged particles
    state.q[4:] = 0.

    #Background ionization
    state.q[e] = 1.e15

    # Leader channel
    ne0 = 1.e21
    state.q[e] += ne0*np.exp( -np.maximum(z-z0,0.)**2/( 2.0 * sigma**2 )
                                - r**2 / (2.0 * sigma**2))

    # Seed streamer inception
    sigmas = sigma/2.
    ne0 = 1.e21
    state.q[e] += ne0*np.exp( -(z-6.1e-2)**2/( 2.0 * sigmas**2 )
                                - r**2 / (2.0 * sigmas**2))

    state.q[O2p] = 0.21 * state.q[e] # N_O2+
    state.q[N2p] = 0.79 * state.q[e] # N_N2+


def auxinit(state):
    """
    aux[0,i,j] = radial coordinate of cell centers for cylindrical source terms
    """
    y = state.grid.y.centers
    for j, r in enumerate(y):
        state.aux[0, :, j] = r

    # Compute charge density and call Poisson equation solver
    dens = np.sum(charge_sign[...,None,None] * state.q[4:] * charge, axis=0)
    rhs = - dens/epsilon_0

    # Potential (Domain decomposition)
    state.problem_data['IG'] = None
    state.problem_data['IGDM'] = None
    state.problem_data['IGDM1'] = None
    edges = True # Whether to perform the calculation of the electric field at the edges or not
    Ez,Er,Eze,Ere = poisson.efield(-rhs, state, edges)


    # Background electric field
    state.problem_data['Ez'] =  - 1.e6 - (2.e6/1.e-6)*state.t
    Ebk = state.problem_data['Ez']

    # Resulting electric field
    Ez += Ebk
    # Electric field at the cell edges
    Eze +=  Ebk # E_{i-1/2,j}

    # Mobilities at cell edges (initially Nair = nair)
    mobility = mu[4]*charge_sign[dspecies['e']]

    # Initialize aux vecs devoted to the electric field and drift velocities
    state.aux[1] = Ez
    state.aux[2] = Er
    state.aux[3] = mobility * Eze[:-1]
    state.aux[4] = mobility * Ere[:,:-1]

    # Drift velocities at rmax and zmax (they will be manually set as boundary conditions)
    state.problem_data['vze'] = mobility * Eze[-1]
    state.problem_data['vre'] = mobility * Ere[:,-1]

    state.problem_data['dN_dt'] = None

    # Dictionary to manage aux writing
    dict_aux = {}
    dict_aux['write'] = True
    dict_aux['num_aux_tbw'] = 2
    dict_aux['list'] = np.array((1,2))
    state.problem_data['set_aux_written'] = dict_aux

    # This is to write into the output files a scalar variable that you can
    # modify every time step
    state.problem_data['derived'] = 0.



def b4step2(solver,state):

    # Compute charge density and call Poisson equation solver
    dens = np.sum(charge_sign[...,None,None] * state.q[4:] * charge, axis=0)
    rhs = - dens/epsilon_0

    # Potential computation (Domain decomposition)
    edges = True
    Ez,Er,Eze,Ere  = poisson.efield(-rhs, state, edges)

    # Background electric field
    state.problem_data['Ez'] = - 1.e6 - (2.e6/1.e-6)*state.t
    Ebk = state.problem_data['Ez'] #+ state.problem_data['capacitor']
    Ez += Ebk
    # Electric field at the cell edges
    Eze += Ebk # E_{i-1/2,j}, i= 0.., j = 0...

    # Mobilities at cell edges
    mobility = mu[4] * charge_sign[dspecies['e']]

    # Initialize aux vecs for the electric field and drift velocities
    state.aux[1] = Ez
    state.aux[2] = Er
    state.aux[3] = mobility * Eze[:-1]
    state.aux[4] = mobility * Ere[:,:-1]

    state.problem_data['vze'] = mobility * Eze[-1]
    state.problem_data['vre'] = mobility * Ere[:,-1]



def dq_cEuler_radial(solver, state, dt):
    """
    This is a Clawpack-style source term routine, which approximates
    the integral of the source terms over a step.
    """

    q = state.q
    aux = state.aux

    # Building a communicator for processes that own the zmax boundary
    ################################################################
    comm_bound = build_comm(state)

    Ez, Er =  aux[1], aux[2]
    dq = dq_comp(solver, state, q, Ez, Er, dt/2., comm_bound)

    qstar = q + dq

    ################Second order ##################################
    # Compute charge density and call Poisson equation solver
    dens = np.sum(charge_sign[...,None,None] * qstar[4:] * charge, axis=0)
    rhs = - dens/epsilon_0

    # Electric field (DDM)
    Ez,Er = poisson.efield(-rhs, state)
    Ez += state.problem_data['Ez']

    dq = dq_comp(solver, state, qstar, Ez, Er, dt, comm_bound)

    comm_bound.Free()

    # In clawpack-style solvers the source terms update q.
    q += dq



def dq_comp(solver, state, q, Ez, Er, dt, comm_bound):

    rad = state.aux[0]

    E = np.array((Ez,Er))

    rho = q[0]
    u   = q[1]/rho
    v   = q[2]/rho
    press  = (gamma - 1.) * ( q[3] - 0.5 * rho * ( u**2 + v**2 ) )

    # Mobility at the cell centers
    Nair = rho / mair
    mobility = mu[4] * charge_sign[dspecies['e']]

    vdriftx = mobility * Ez
    vdrifty = mobility * Er

    vel_u = u + vdriftx
    vel_v = v + vdrifty
    vel = np.array((vel_u,vel_v)) # Space component first and specie component after

    # Current density
    jz = charge * vel[0] * q[4] * charge_sign[dspecies['e']]
    jr = charge * vel[1] * q[4] * charge_sign[dspecies['e']]
    j = np.array((jz,jr))

    # Local dissipated energy from the electric discharge
    w = eta * np.sum(j*E, axis = 0)

    # Reduced electric field
    Emod = np.sqrt(Ez**2 + Er**2)
    Ered = (Emod/Nair) * (1e21) # Townsend
    tgas = press / (co.k * Nair)

    # Vector encoding dn/dt for each species
    super_dn_dt = chemistry.chemical(state, Ered, tgas) # dn/dt

    dq = np.empty(q.shape)

    dq[0] = -dt/rad * q[2]
    dq[1] = -dt/rad * rho*u*v
    dq[2] = -dt/rad * rho*v*v

    dq[3] = (-dt/rad * v * (q[3] + press)
                 + dt * w)

    dq[5:] = -dt/rad * (q[5:]*v[None,...]) + super_dn_dt[1:] * dt
    dq[4] = -dt/rad * (q[4]*vel[1]) + super_dn_dt[0] * dt + diff(state,solver) * dt

    # Save dNe_dt
    state.problem_data['dN_dt'] = np.abs(super_dn_dt)


    return dq



def diff(state,solver):

    ye = state.grid.y.nodes
    r = state.aux[0]

    D = 0.18

    qbc = solver.qbc[4].copy()

    # Built upon two ghost cells
    if state.grid.x.nodes[-1] == bz:
        qbc[-1,2:-2] = qbc[-3,2:-2]
        qbc[-2,2:-2] = qbc[-3,2:-2]

    if state.grid.x.nodes[0] == az:
        qbc[0,2:-2] = qbc[2,2:-2]
        qbc[1,2:-2] = qbc[2,2:-2]

    if state.grid.y.nodes[-1] == br:
        qbc[2:-2,-1] = qbc[2:-2,-3]
        qbc[2:-2,-2] = qbc[2:-2,-3]

    if state.grid.y.nodes[0] == ar:
        qbc[2:-2,0] = qbc[2:-2,3]
        qbc[2:-2,1] = qbc[2:-2,2]


    # This derivatives has been build upon two ghost cells
    dn_dr_r = (qbc[2:-2,3:-1] - qbc[2:-2,2:-2])/dr # Right edge
    dn_dr_l = (qbc[2:-2,2:-2] - qbc[2:-2,1:-3])/dr # Left edge


    d2n_dz2 = (qbc[3:-1,2:-2] - 2.*qbc[2:-2,2:-2] + qbc[1:-3,2:-2])/dz**2


    f = (-ye[None,1:] * D * dn_dr_r/dr +

            ye[None,:-1] * D * dn_dr_l/dr )/r - D * d2n_dz2


    return - f



def build_comm(state):
    world_rank = MPI.COMM_WORLD.Get_rank()
    zf = state.grid.x.nodes
    if zf[-1] == bz :
        color = 55
        key = 0
    else:
        color = 77
        key = 0

    comm_bound = MPI.COMM_WORLD.Split(color,key)

    return comm_bound



def interp(x,y,z):
    return interpolate.RectBivariateSpline(x,y,z)



def lower_radial_bc(state,dim,t,qbc,auxbc,num_ghost):

    # Mirror
    for i in range(num_ghost):
        qbc[:,:,i] = qbc[:,:,num_ghost + 1 - i]
        qbc[y_momentum,:,i] = - qbc[y_momentum,:,num_ghost + 1 - i]

def lower_radial_auxbc(state,dim,t,qbc,auxbc,num_ghost):

    # Mirror
    for i in range(num_ghost):
        auxbc[:,:,i] = auxbc[:,:,num_ghost + 1 - i]
        auxbc[2,:,i] = - auxbc[2,:,num_ghost +1 - i]
        auxbc[nspecies + 3: 2*nspecies + 3,:,i] = - auxbc[nspecies + 3: 2*nspecies + 3,:,num_ghost + 1 - i]

def upper_auxbc(state,dim,t,qbc,auxbc,num_ghost):

    # Extrapolation
    for i in range(num_ghost):
        auxbc[:,-num_ghost + i] = auxbc[:,-num_ghost - 1]
        auxbc[3:nspecies + 3,-num_ghost + i,num_ghost:-num_ghost] = state.problem_data['vze']
        auxbc[:,:,-num_ghost + i] = auxbc[:,:,-num_ghost - 1]
        auxbc[3:nspecies + 3,num_ghost:-num_ghost,-num_ghost + i] = state.problem_data['vre']



def set_time_step(solver,solution):
    # This function modifies the time step by looking at the smallest
    # of the set {dt_clawpack,t_maxwell,t_chemical}
    state = solution.state

    cfl = solver.cfl.get_cached_max()
    dt_clawpack = min(solver.dt_max,solver.dt * solver.cfl_desired / cfl)

    # Maxwell relaxation time
    sigma = state.q[e] * charge * mu[e]
    t_maxwell = co.epsilon_0/sigma
    t_maxwell = t_maxwell*(t_maxwell>0.)
    t_maxwell = t_maxwell[np.nonzero(t_maxwell)]
    t_maxwell = np.nanmin(t_maxwell)

    dN_dt = state.problem_data['dN_dt']

    if dN_dt is None: t_chemical = t_maxwell

    else:
        # Chemical relaxation time
        inv_tau = dN_dt/state.q[4:]
        t_chemical = 1./inv_tau
        t_chemical = np.isfinite(t_chemical)
        t_chemical = t_chemical*(t_chemical>0.)
        t_chemical = t_chemical[np.nonzero(t_chemical)]
        t_chemical = np.nanmin(t_chemical)

    mintime = min(t_chemical,t_maxwell)
    mintimes = poisson.scattertoall(mintime,MPI.COMM_WORLD)
    mintime = np.amin(mintimes)

    solver.dt = min(mintime,dt_clawpack)



def setup(use_petsc=False, solver_type='classic', outdir='_output',
          kernel_language='Fortran', disable_output=False, mx=dimx, my=dimy, tfinal=5.e-6,
          num_output_times = 5000, htmlplot=False, tfluct_solver=False):

    if use_petsc:
        import clawpack.petclaw as pyclaw
    else:
        from clawpack import pyclaw

    # We have not used the sharpclaw solver type
    if solver_type=='sharpclaw':
        solver = pyclaw.SharpClawSolver2D(euler_drift)
        solver.dq_src = dq_Euler_radial
        solver.call_before_step_each_stage = True
        solver.before_step = b4step2
        solver.weno_order = 5
        solver.lim_type   = 2
        #solver.cfl_desired = 1e-8
        solver.cfl_max = 1e-2
        solver.dt_initial = 1e-12 # Set initial time step
        #solver.dt_variable = True
        #solver.dt_max = 10
        solver.tfluct_solver = tfluct_solver
        if solver.tfluct_solver:
            import euler_tfluct
            solver.tfluct = euler_tfluct

    else:
        solver = pyclaw.ClawSolver2D(euler_drift)
        solver.step_source = dq_cEuler_radial
        solver.source_split = 1
        solver.limiters = 4
        solver.cfl_max = 0.01
        solver.cfl_desired = 0.005
        solver.before_step = b4step2
        solver.dimensional_split = True
        solver.order = 2
        solver.dt_variable = True
        solver.dt_initial = 1.e-12
        solver.get_dt_new = set_time_step

    solver.user_aux_bc_lower = lower_radial_auxbc
    solver.user_bc_lower = lower_radial_bc
    solver.user_aux_bc_upper = upper_auxbc

    solver.bc_lower[0]=pyclaw.BC.extrap
    solver.bc_upper[0]=pyclaw.BC.extrap
    solver.bc_lower[1]=pyclaw.BC.custom
    solver.bc_upper[1]=pyclaw.BC.extrap
    #Aux variable in ghost cells doesn't matter
    solver.aux_bc_lower[0]=pyclaw.BC.extrap
    solver.aux_bc_upper[0]=pyclaw.BC.custom
    solver.aux_bc_lower[1]= pyclaw.BC.custom
    solver.aux_bc_upper[1]=pyclaw.BC.custom

    claw = pyclaw.Controller()
    claw.solver = solver

    # restart options
    restart_from_frame = None

    if restart_from_frame is None:
        x = pyclaw.Dimension(az, bz, mx, name='x')
        y = pyclaw.Dimension(ar, br, my, name='y')
        domain = pyclaw.Domain([x, y])

        # I don't understand why this was not needed in the example code. AL.
        solver.num_waves = num_eqn
        solver.num_eqn = num_eqn
        solver.max_steps = 100000
        num_aux = 5

        state = pyclaw.State(domain, num_eqn, num_aux)
        state.problem_data['gamma'] = gamma
        qinit(state)
        auxinit(state)
        claw.solution = pyclaw.Solution(state,domain)
        claw.num_output_times = num_output_times
    else:
        solver.num_waves = num_eqn
        solver.num_eqn = num_eqn
        solver.max_steps = 100000
        claw.solution = pyclaw.Solution(restart_from_frame,file_format='hdf5',read_aux=False)
        claw.solution.state.problem_data['gamma'] = gamma
        auxinit(claw.solution.state)
        claw.start_frame = restart_from_frame
        claw.num_output_times = num_output_times - restart_from_frame

    claw.keep_copy = False
    if disable_output:
        claw.output_format = None
    else: claw.output_format = 'hdf5'

    claw.tfinal = tfinal
    claw.outdir = outdir
    # This is to manage writing of aux variables
    claw.write_aux_init = True
    claw.write_aux_always = True


    return claw



if __name__=="__main__":
    from clawpack.pyclaw.util import run_app_from_main
    output = run_app_from_main(setup)
