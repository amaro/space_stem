# SPACE STEM

## What is SPACE STEM?

Sample code to generate the output data that support our findings. You can reproduce
the spontaneous emergence of space stems ahead of a negative leader. This code consists
of:

* **CLAWPACK:**
  [Clawpack](http://www.clawpack.org/) (“Conservation Laws Package”)
  is a collection of finite volume methods for linear and
  nonlinear hyperbolic systems of conservation laws.
  Clawpack employs high-resolution Godunov-type methods with limiters
  in a general framework applicable to many kinds of waves.

* **/space_stem/poisson_sparse.py:**
  It is a solver (MPI-parallelization) for the Poisson equation.

* **/space_stem/rpn2_euler_drift.f90:**
  Roe-solver for the Euler equations + drift-diffusion-reaction equations.

* **/space_stem/default.py:**
  Code that uses CHEMISE to evaluate our chemical system.


## Requeriments
To run this code, first you need to install:

- [Clawpack](http://www.clawpack.org/)
- [CHEMISE](https://gitlab.com/aluque/chemise.git)
- [PETSC](https://www.mcs.anl.gov/petsc/)
- [MPI4Py](http://mpi4py.scipy.org/docs/)
- [petsc4py](https://bitbucket.org/petsc/petsc4py)
